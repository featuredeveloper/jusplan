Rails.application.routes.draw do
  devise_for :users, :path_prefix => 'my'
  devise_scope :user do
    get '/login' => 'devise/sessions#new'
    get '/logout' => 'devise/sessions#destroy'
  end

  resources :users, :controller => "users"
  resources :desempenhos
  resources :orgaos
  resources :magistrados
  resources :desembargadores

  get 'desempenho_vazio' => 'desempenhos#desempenho_vazio'
  get 'listar_desempenhos' => 'desempenhos#listar_desempenhos'
  get 'desempenhos_magistrados' => 'desempenhos#desempenhos_magistrados'
  get 'desempenhos_orgaos' => 'desempenhos#desempenhos_orgaos'
  post 'gerar_desempenho' => 'desempenhos#gerar_desempenho'
  post 'bloquear/:id' => 'desempenhos#bloquear', as: 'bloquear'
  post 'desbloquear/:id' => 'desempenhos#desbloquear', as: 'desbloquear'
  root to: 'desempenhos#index'

end







