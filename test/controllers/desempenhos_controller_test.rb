require 'test_helper'

class DesempenhosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @desempenho = desempenhos(:one)
  end

  test "should get index" do
    get desempenhos_url
    assert_response :success
  end

  test "should get new" do
    get new_desempenho_url
    assert_response :success
  end

  test "should create desempenho" do
    assert_difference('Desempenho.count') do
      post desempenhos_url, params: { desempenho: { acordos_homologados: @desempenho.acordos_homologados, decisoes: @desempenho.decisoes, decisoes_557: @desempenho.decisoes_557, desembargador_id: @desempenho.desembargador_id, distribuidos: @desempenho.distribuidos, julgamentos_com_merito: @desempenho.julgamentos_com_merito, julgamentos_sem_merito: @desempenho.julgamentos_sem_merito, magistrado_id: @desempenho.magistrado_id, orgao_id: @desempenho.orgao_id, redistribuidos: @desempenho.redistribuidos, residuos: @desempenho.residuos } }
    end

    assert_redirected_to desempenho_url(Desempenho.last)
  end

  test "should show desempenho" do
    get desempenho_url(@desempenho)
    assert_response :success
  end

  test "should get edit" do
    get edit_desempenho_url(@desempenho)
    assert_response :success
  end

  test "should update desempenho" do
    patch desempenho_url(@desempenho), params: { desempenho: { acordos_homologados: @desempenho.acordos_homologados, decisoes: @desempenho.decisoes, decisoes_557: @desempenho.decisoes_557, desembargador_id: @desempenho.desembargador_id, distribuidos: @desempenho.distribuidos, julgamentos_com_merito: @desempenho.julgamentos_com_merito, julgamentos_sem_merito: @desempenho.julgamentos_sem_merito, magistrado_id: @desempenho.magistrado_id, orgao_id: @desempenho.orgao_id, redistribuidos: @desempenho.redistribuidos, residuos: @desempenho.residuos } }
    assert_redirected_to desempenho_url(@desempenho)
  end

  test "should destroy desempenho" do
    assert_difference('Desempenho.count', -1) do
      delete desempenho_url(@desempenho)
    end

    assert_redirected_to desempenhos_url
  end
end
