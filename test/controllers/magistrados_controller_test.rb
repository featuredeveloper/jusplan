require 'test_helper'

class MagistradosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @magistrado = magistrados(:one)
  end

  test "should get index" do
    get magistrados_url
    assert_response :success
  end

  test "should get new" do
    get new_magistrado_url
    assert_response :success
  end

  test "should create magistrado" do
    assert_difference('Magistrado.count') do
      post magistrados_url, params: { magistrado: { genero: @magistrado.genero, name: @magistrado.name, orgaos_id: @magistrado.orgaos_id } }
    end

    assert_redirected_to magistrado_url(Magistrado.last)
  end

  test "should show magistrado" do
    get magistrado_url(@magistrado)
    assert_response :success
  end

  test "should get edit" do
    get edit_magistrado_url(@magistrado)
    assert_response :success
  end

  test "should update magistrado" do
    patch magistrado_url(@magistrado), params: { magistrado: { genero: @magistrado.genero, name: @magistrado.name, orgaos_id: @magistrado.orgaos_id } }
    assert_redirected_to magistrado_url(@magistrado)
  end

  test "should destroy magistrado" do
    assert_difference('Magistrado.count', -1) do
      delete magistrado_url(@magistrado)
    end

    assert_redirected_to magistrados_url
  end
end
