class Orgao < ApplicationRecord
    has_and_belongs_to_many :magistrados
    has_and_belongs_to_many :users
    has_many :desempenhos
end
