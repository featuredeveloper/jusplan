class Desempenho < ApplicationRecord
  belongs_to :orgao
  belongs_to :magistrado
  belongs_to :user
  # before_save :calcular_residuos
  before_create :setData, on: :create
  validates_presence_of :decisoes, :decisoes_557, :acordos_homologados, :julgamentos_com_merito, :julgamentos_sem_merito, :distribuidos, :redistribuidos, :magistrado_id, :orgao_id, message: "Campo obrigatório."
  validates_numericality_of :decisoes, :decisoes_557, :acordos_homologados, :julgamentos_com_merito, :julgamentos_sem_merito, :distribuidos, :redistribuidos, :greater_than_or_equal_to => 0, message: "Valor deve ser maior que zero."
  validate :validate_magistrado_have_orgao
  validate :validate_user_have_orgao
  # validate :validate_saldo_residuo
  # validate :validate_creation_date, on: :create
  # validate :validate_uniqueness, on: :create
  validate :validar_pendencias, on: :save

  def self.search(search)
    if search
      where(:data => Time.new(2017, search))
    else
      all
    end
  end

  protected

  def calcular_residuos
    julgamentosComMerito = self.julgamentos_com_merito
    julgamentosSemMerito = self.julgamentos_sem_merito
    acordosHomologados = self.acordos_homologados
    distribuidos = self.distribuidos
    self.residuos = distribuidos - (julgamentosComMerito + julgamentosSemMerito + acordosHomologados)
  end

  def setData
    unless self.data?
      time = Time.now - 1.month
      self.data = Time.new(time.year, time.month)
    else
      self.ativo = false
    end
  end

  private

  def validate_magistrado_have_orgao
    magistrado = Magistrado.where(:id => self.magistrado_id).joins(:orgaos).where(:orgaos => {:id => self.orgao})
    errors.add(:orgao_id, 'Orgão não pertecente a este magistrado.') unless magistrado.exists?
  end

  def validate_creation_date
      errors.add(:orgao_id, 'Data disponivel para lançamentos apenas entre os dias 1-15.') unless Time.now.strftime("%d").to_i <= 15
  end

  def validate_uniqueness
    time = Time.now - 1.month
    time = Time.new(time.year, time.month)
    desempenho = Desempenho.where(:data => time).joins(:orgao).where(:orgaos => {:id => self.orgao}).joins(:magistrado).where(:magistrados => {:id => self.magistrado})
    errors.add(:orgao_id, 'Lançamento já realizado.') if desempenho.exists?
  end

  def validate_user_have_orgao
    user = User.where(:id => self.user_id).joins(:orgaos).where(:orgaos => {:id => self.orgao})
    errors.add(:orgao_id, 'Você não tem acesso a este orgão') unless user.exists?
  end

  def validate_saldo_residuo
    julgamentosComMerito = self.julgamentos_com_merito
    julgamentosSemMerito = self.julgamentos_sem_merito
    acordosHomologados = self.acordos_homologados
    distribuidos = self.distribuidos
    residuoAtual =  distribuidos - (julgamentosComMerito + julgamentosSemMerito + acordosHomologados)
    residuoAnterior = Desempenho.all.where.not(:id => self.id, :ativo => false).joins(:orgao, :magistrado).where({:magistrados => {:id => self.magistrado_id}}).where({:orgaos => {:id => self.orgao_id}}).sum(:residuos)
    residuo = (residuoAtual + residuoAnterior)
    return self.errors.add(:base, "Valor Incompativel!") if residuo < 0
  end

  def validar_pendencias
    desempenhos = Desempenho.all.where(:ativo => true).joins(:orgao, :magistrado).where({:magistrados => {:id => self.magistrado_id}}).where({:orgaos => {:id => self.orgao_id}})
    return self.errors.add(:base, "Existem Desempenhos Pendentes a serem editados") if desempenhos.exists? && self.ativo != true
  end
end
