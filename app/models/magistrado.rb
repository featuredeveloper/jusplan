class Magistrado < ApplicationRecord
  before_update :magistrado_prefixo
  has_and_belongs_to_many :orgaos
  has_many :desempenhos

  enum genero: [:Masculino, :Feminino]
  enum cargo: [:Magistrado, :Desembargador]

  protected

    def magistrado_prefixo
      genero = self.genero
      cargo = self.cargo

      if genero == 'Masculino' && cargo == 'Desembargador'
        self.prefixo = 'Des.'
      elsif genero == 'Feminino' && cargo == 'Desembargador'
        self.prefixo = 'Desª.'
      elsif genero == 'Masculino' && cargo == 'Magistrado'
        self.prefixo = 'Mmº.'
      else
        self.prefixo = 'Mmª.'
      end

    end
end
