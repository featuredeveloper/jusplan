class Ability
  include CanCan::Ability


    def initialize(user)
      user ||= User.new
        if user.role == "admin"
          can :manage, :all
        elsif user.role == "user"
          can :read, Orgao
          can :read, Magistrado
          can [:destroy, :read, :create], Desempenho
          can [:update], Desempenho, :ativo => false
          can :update, User, :id => user.id
        end
    end
end
