class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable
  enum role: [:admin, :user]
  has_and_belongs_to_many :orgaos
  has_many :desempenhos
  before_create :set_username

  protected

    def set_username
      if self.username?
        self.username.downcase!
      else
        aux = self.name.split
        aux = [aux.first, aux.last].join(".")
        self.username = aux.downcase
      end

    end
end
