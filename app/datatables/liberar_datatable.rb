class LiberarDatatable < AjaxDatatablesRails::Base
=begin
  def_delegator :@view, :link_to

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      orgao: { source: "Desempenho.orgao_id", cond: :like },
      magistrado: { source: "Desempenho.Magistrado_id", cond: :like },
      data: { source: "Desempenho.data", cond: :like },
      ativo: { source: "Desempenho.ativo", cond: :like }
    }
  end

  def data
    records.map do |record|
      {
        orgao: record.orgao.name,
        magistrado: record.magistrado.name,
        data: record.data,
        ativo: record.ativo == true ? link_to('Desbloquear', @view.desbloquear_path(record.id), :method => :post, class: 'btn btn-success btn-sm', remote: true) : link_to('Bloquear', @view.bloquear_path(record.id), :method => :post, class: 'btn btn-warning btn-sm', remote: true),
      }
    end
  end

  private

  def get_raw_records
    Desempenho.all
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
=end
end
