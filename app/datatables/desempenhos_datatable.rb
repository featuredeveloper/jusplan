class DesempenhosDatatable < AjaxDatatablesRails::Base
=begin
def_delegator :@view, :link_to

  def view_columns
    @view_columns ||= {
      orgao: { source: "Desempenho.orgao_id", cond: :like},
      magistrado: { source: "Desempenho.Magistrado_id", cond: :like},
      data: { source: "Desempenho.data" },
      decisoes: { source: "Desempenho.decisoes", cond: :like },
      decisoes_557: { source: "Desempenho.decisoes_557", cond: :like },
      acordos_homologados: { source: "Desempenho.acordos_homologados", cond: :like },
      julgamentos_com_merito: { source: "Desempenho.julgamentos_com_merito", cond: :like },
      julgamentos_sem_merito: { source: "Desempenho.julgamentos_sem_merito", cond: :like },
      distribuidos: { source: "Desempenho.distribuidos", cond: :like },
      redistribuidos: { source: "Desempenho.redistribuidos", cond: :like },
      residuos: { source: "Desempenho.residuos", cond: :like },
      editar: "editar"
    }
  end

  private

  def data
    records.map do |record|
      {
        orgao: record.orgao.name,
        magistrado: record.magistrado.name,
        data: record.data,
        decisoes: record.decisoes,
        decisoes_557: record.decisoes_557,
        acordos_homologados: record.acordos_homologados,
        julgamentos_com_merito: record.julgamentos_com_merito,
        julgamentos_sem_merito: record.julgamentos_sem_merito,
        distribuidos: record.distribuidos,
        redistribuidos: record.redistribuidos,
        residuos: record.residuos,
        editar: record.ativo == true ?  link_to("Editar", @view.edit_desempenho_path(record.id), class: "btn btn-primary btn-sm", remote: true) : "Bloqueado" #Inverta isso aqui ao acabar
      }
    end
  end

  def get_raw_records
    Desempenho.joins(:magistrado, :orgao)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
=end
end
