class ApplicationController < ActionController::Base


  before_action :configure_permitted_parameters, if: :devise_controller?

    rescue_from CanCan::AccessDenied do |exception|
        flash[:error] = exception.message
        redirect_to login_path
    end

  protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :username, :role, :email, :password, :password_confirmation, orgao_ids:[]])
        devise_parameter_sanitizer.permit(:sign_in, keys: [:username, :password])
    end
end
