class MagistradosController < ApplicationController
  load_and_authorize_resource
  before_action :set_magistrado, only: [:show, :edit, :update, :destroy]

  # GET /magistrados
  def index
    @magistrados = Magistrado.all
  end

  # GET /magistrados/new
  def new
    @magistrado = Magistrado.new
  end

  # GET /magistrados/1/edit
  def edit
  end

  # POST /magistrados
  def create
    @magistrado = Magistrado.new(magistrado_params)
    if @magistrado.save
      redirect_to @magistrados, notice: 'Magistrado criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /magistrados/1
  def update
    if @magistrado.update(magistrado_params)
      redirect_to @magistrados, notice: 'Magistrado editado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /magistrados/1
  def destroy
    @magistrado.destroy
    redirect_to magistrados_url, notice: 'Magistrado deletado com sucesso.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_magistrado
      @magistrado = Magistrado.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def magistrado_params
      params.require(:magistrado).permit(:name, :genero, :cargo, orgao_ids:[])
    end
end
