class UsersController < ApplicationController
  load_and_authorize_resource
  before_action :set_user, only: [:edit, :show, :update, :destroy]

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
        flash[:notice] = "Usuário criado com sucesso."
        redirect_to users_path
    else
        render action: 'new'
    end
  end

  def edit
  end

  def update
    params[:user].delete(:password) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
    if @user.update(update_params)
      flash[:notice] = "Usuário editado com sucesso."
      redirect_to users_path
    else
      flash[:notice] = "Erro ao criar."
      render 'new'
    end
  end

  def destroy
    if @user.destroy
        flash[:notice] = "Usuário deletado com sucesso."
        redirect_to users
    end
  end

  protected

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:confirmation_token, :confirmed_at, :confirmed_sent_at, :unconfirmed_email, :username, :password, :password_confirmation, :name, :email, :role, orgao_ids:[])
  end

  def update_params
    params.require(:user).permit(:confirmation_token, :confirmed_at, :confirmed_sent_at, :unconfirmed_email, :username, :name, :email, :role, orgao_ids:[])
  end

end
