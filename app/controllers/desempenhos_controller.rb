class DesempenhosController < ApplicationController
  load_and_authorize_resource
  before_action :set_desempenho, only: [:edit, :show, :update, :destroy]

  def index
    @desempenhos = Desempenho.where(ativo: true).group(:data)
    @grafico = Desempenho.where(ativo: true).group(:data).sum(:residuos)
  end

  def desempenho_vazio
    redirect_to desempenhos_path, notice: 'Acesso negado' unless current_user.role === 'admin'
    @desempenho = Desempenho.new
  end

  def gerar_desempenho
    redirect_to desempenhos_path, notice: 'Acesso negado' unless current_user.role === 'admin'
    @desempenho = Desempenho.new(desempenho_params)
    @desempenho.user_id = current_user.id if current_user
    @desempenho.decisoes = 0
    @desempenho.decisoes_557 = 0
    @desempenho.acordos_homologados = 0
    @desempenho.julgamentos_com_merito = 0
    @desempenho.julgamentos_sem_merito = 0
    @desempenho.distribuidos = 0
    @desempenho.redistribuidos = 0
    @desempenho.ativo = false
    if @desempenho.save!
      redirect_to desempenhos_path, notice: 'Desempenho Criado Com Sucesso.'
    else
      render :desempenho_vazio
    end
  end

  def listar_desempenhos
    @desempenhos = Desempenho.all
  end

  def desempenhos_magistrados
    @desempenhos = Desempenho.search(params[:mes]).group_by(&:magistrado)
    @grafico = Magistrado.all.map {|magistrado| {
      name: magistrado.name,
      data: magistrado.desempenhos.where(ativo: true).group(:data).sum(:residuos)
    }}
  end

  def desempenhos_orgaos
    @desempenhos = Desempenho.search(params[:mes]).group_by(&:orgao)
    @grafico = Orgao.all.map {|orgao| {
      name: orgao.name,
      data: orgao.desempenhos.where(ativo: true).group(:data).sum(:residuos)
    }}
  end

  def show
  end

  def new
    @desempenho = Desempenho.new
  end

  def edit
  end

  def create
    @desempenho = Desempenho.new(desempenho_params)
    @desempenho.user_id = current_user.id if current_user
    if @desempenho.save
      redirect_to desempenhos_url, notice: 'Desempenho criado com sucesso.'
    else
      render :new
    end
  end

  def update
      if @desempenho.update(desempenho_params)
        redirect_to desempenhos_url, notice: 'Desempenho editado com sucesso.'
      else
        render :edit
      end
  end

  def bloquear
    Desempenho.update((params[:id]), :ativo => true)
  end
  helper_method :bloquear

  def desbloquear
    Desempenho.update((params[:id]), :ativo => false)
  end
  helper_method :desbloquear

  def destroy
    @desempenho.destroy
    redirect_to desempenhos_url, notice: 'Desempenho destruido com sucesso.'
  end

  private

  def set_desempenho
    @desempenho = Desempenho.find(params[:id])
  end

  def desempenho_params
    params.require(:desempenho).permit(:user_id, :orgao_id, :magistrado_id, :decisoes, :decisoes_557, :acordos_homologados, :julgamentos_com_merito, :julgamentos_sem_merito, :distribuidos, :redistribuidos, :residuos, :ativo, :mes, :data)
  end
end
