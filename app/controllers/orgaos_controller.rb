class OrgaosController < ApplicationController
  load_and_authorize_resource
  before_action :set_orgao, only: [:show, :edit, :update, :destroy]

  # GET /orgaos
  def index
    @orgaos = Orgao.all
  end

  # GET /orgaos/new
  def new
    @orgao = Orgao.new
  end

  # GET /orgaos/:id/edit
  def edit
  end

  # POST /orgaos
  def create
    @orgao = Orgao.new(orgao_params)
    if @orgao.save
      redirect_to @orgao, notice: 'Orgao criado com sucesso.'
    else
      render :new
    end
  end

  # PATCH/PUT /orgaos/:id
  def update
    if @orgao.update(orgao_params)
      redirect_to @orgao, notice: 'Orgao editado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /orgaos/:id
  def destroy
    @orgao.destroy
      redirect_to orgaos_url, notice: 'Orgao deletado com sucesso.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orgao
      @orgao = Orgao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orgao_params
      params.require(:orgao).permit(:name)
    end
end
