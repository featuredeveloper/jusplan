CREATE TABLE "schema_migrations" ("version" varchar NOT NULL PRIMARY KEY);
CREATE TABLE "ar_internal_metadata" ("key" varchar NOT NULL PRIMARY KEY, "value" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "orgaos" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "magistrados" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "genero" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "desembargadores" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "name" varchar, "genero" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "desempenhos" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "decisoes" integer, "decisoes_557" integer, "acordos_homologados" integer, "julgamentos_com_merito" integer, "julgamentos_sem_merito" integer, "distribuidos" integer, "redistribuidos" integer, "residuos" integer, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "magistrados_orgaos" ("orgao_id" integer NOT NULL, "magistrado_id" integer NOT NULL);
INSERT INTO "schema_migrations" (version) VALUES
('20170523220748'),
('20170523221424'),
('20170523221600'),
('20170523221847'),
('20170524215047'),
('20170524222315');


