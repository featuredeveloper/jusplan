# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170828165014) do

  create_table "desempenhos", force: :cascade do |t|
    t.integer  "decisoes"
    t.integer  "decisoes_557"
    t.integer  "acordos_homologados"
    t.integer  "julgamentos_com_merito"
    t.integer  "julgamentos_sem_merito"
    t.integer  "distribuidos"
    t.integer  "redistribuidos"
    t.integer  "residuos"
    t.datetime "data"
    t.boolean  "ativo",                  default: true, null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "magistrado_id"
    t.integer  "orgao_id"
    t.integer  "user_id"
    t.index ["magistrado_id"], name: "index_desempenhos_on_magistrado_id"
    t.index ["orgao_id"], name: "index_desempenhos_on_orgao_id"
    t.index ["user_id"], name: "index_desempenhos_on_user_id"
  end

  create_table "magistrados", force: :cascade do |t|
    t.string   "name"
    t.integer  "genero"
    t.integer  "cargo"
    t.string   "prefixo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "magistrados_orgaos", id: false, force: :cascade do |t|
    t.integer "orgao_id",      null: false
    t.integer "magistrado_id", null: false
  end

  create_table "orgaos", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_orgaos_on_user_id"
  end

  create_table "orgaos_users", id: false, force: :cascade do |t|
    t.integer "orgao_id", null: false
    t.integer "user_id",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "username"
    t.integer  "role"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
