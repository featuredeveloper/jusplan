class AddUserToDesempenho < ActiveRecord::Migration[5.0]
  def change
    add_reference :desempenhos, :user, foreign_key: true
  end
end
