class CreateMagistrados < ActiveRecord::Migration[5.0]
  def change
    create_table :magistrados do |t|
      t.string :name
      t.integer :genero
      t.integer :cargo
      t.string :prefixo
      t.timestamps
    end
  end
end
