class CreateDesempenhos < ActiveRecord::Migration[5.0]
  def change
    create_table :desempenhos do |t|
      t.integer :decisoes
      t.integer :decisoes_557
      t.integer :acordos_homologados
      t.integer :julgamentos_com_merito
      t.integer :julgamentos_sem_merito
      t.integer :distribuidos
      t.integer :redistribuidos
      t.integer :residuos
      t.datetime :data
      t.boolean :ativo, null: false, default: true

      t.timestamps
    end
  end
end
