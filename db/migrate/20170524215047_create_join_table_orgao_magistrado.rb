class CreateJoinTableOrgaoMagistrado < ActiveRecord::Migration[5.0]
  def change
    create_join_table :orgaos, :magistrados do |t|
      # t.index [:orgao_id, :magistrado_id]
      # t.index [:magistrado_id, :orgao_id]
    end
  end
end
