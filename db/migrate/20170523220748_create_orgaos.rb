class CreateOrgaos < ActiveRecord::Migration[5.0]
  def change
    create_table :orgaos do |t|
      t.string :name

      t.timestamps
    end
  end
end
