class CreateJoinTableOrgaoUser < ActiveRecord::Migration[5.0]
  def change
    create_join_table :orgaos, :users do |t|
      # t.index [:orgao_id, :user_id]
      # t.index [:user_id, :orgao_id]
    end
  end
end
