class AddMagistradoAndOrgaoToDesempenho < ActiveRecord::Migration[5.0]
  def change
    add_reference :desempenhos, :magistrado, foreign_key: true
    add_reference :desempenhos, :orgao, foreign_key: true
  end
end
