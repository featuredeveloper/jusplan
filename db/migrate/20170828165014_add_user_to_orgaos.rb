class AddUserToOrgaos < ActiveRecord::Migration[5.0]
  def change
    add_reference :orgaos, :user, foreign_key: true
  end
end
