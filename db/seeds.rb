User.delete_all
Magistrado.delete_all
Orgao.delete_all

Orgao.create([
    {
        id: 1,
        name: "Pleno"
    },
    {
        id: 2,
        name: "1ª Câmara Civil"
    },
    {
        id: 3,
        name: "2ª Câmara Civil"
    },
    {
        id: 4,
        name: "Câmara Criminal"
    },
    {
        id: 5,
        name: "1ª Turma Recursal"
    },
    {
        id: 6,
        name: "2ª Turma Recursal"
    },
])

User.create([
    {
        id: 1,
        name: "John Doe",
        role: "admin",
        email: "jd@email.com",
        password: 123456,
        orgao_ids: [1, 2, 3, 4, 5, 6]
    },
    {
        id: 2,
        name: "admin",
        role: "admin",
        email: "admin@email.com",
        password: 123456,
        orgao_ids: [1, 2, 3, 4, 5, 6]
    },
    {
        id: 3,
        name: "Man With No Name",
        role: "user",
        email: "noname@email.com",
        password: 123456,
        orgao_ids: [1, 2, 3]
    },
    {
        id: 4,
        name: "Nobody's woman",
        role: "user",
        email: "nobody@email.com",
        password: 123456,
        orgao_ids: [4, 5, 6]
    }
])

Magistrado.create([
    {
        id: 1,
        name: 'Dredd',
        genero: 'Masculino',
        cargo: 'Desembargador',
        orgao_ids: [1, 2, 3, 4]
    },
    {
        id: 2,
        name: 'Nope',
        genero: 'Feminino',
        cargo: 'Magistrado',
        orgao_ids: [5, 6]
    }
])

    Desempenho.create([
        {
            magistrado_id: 1,
            orgao_id: 1,
            user_id: 1,
            distribuidos: 20,
            julgamentos_com_merito: 10,
            julgamentos_sem_merito: 10,
            acordos_homologados: 5,
            decisoes: 10,
            decisoes_557: 10,
        }
    ])



    Desempenho.create([
        {
            magistrado_id: 2,
            orgao_id: 4,
            user_id: 1,
            distribuidos: 20,
            julgamentos_com_merito: 10,
            julgamentos_sem_merito: 10,
            acordos_homologados: 5,
            decisoes: 10,
            decisoes_557: 10,
        }
    ])
